import 'Cell.dart';
import '../interfaces/IFrequency.dart';
import '../util/Ore.dart';

/**
 * A mine is a place where the player can dig resources
 */
abstract class Mine<T extends Cell> extends Cell implements IFrequency<T>{

  /**
   * The quantity of ores in the mine
   */
  int resourceQuantity;

  /**
   * Tells if the mine is empty or not
   */
  bool isEmpty;

  /**
   * Class constructor
   */
  Mine(this.resourceQuantity, int x, int y): super(x,y) {
    isEmpty = resourceQuantity <= 0;
  }

  /**
   * Mines the resources of the mine and returns the number of ore mined.
   */
  int mineOre(int canBeMined) {
    int count = getMinedCount();
    int nbrOre = 0;
    if (!isEmpty && canBeMined > 0) {
      if (resourceQuantity >= count && canBeMined >= count) {
        nbrOre = count;
      }
      else if ((resourceQuantity >= count && canBeMined <= count) || (resourceQuantity < count && resourceQuantity >= canBeMined ) ) {
        nbrOre = canBeMined;
      }
       else {
        nbrOre = resourceQuantity;
      }
      resourceQuantity -= nbrOre;
      if (resourceQuantity == 0) {
        isEmpty = true;
      }
    }
    return nbrOre;
  }

  /**
   * Returns the number of ore that can be mined
   */
  int getMinedCount();

  /**
   * Returns the ore type
   */
  Ore getOreType();
}