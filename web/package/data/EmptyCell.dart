import 'Cell.dart';
import '../interfaces/IFrequency.dart';

/**
 * An empty cell is an area without anything on it
 */
class EmptyCell extends Cell implements IFrequency<EmptyCell> {

  /**
   * The path of the cell image
   */
  static const _IMG_PATH = "images/empty.png";

  /**
   * The name of the resource
   */
  static const _NAME = "empty";

  /**
   * Class constructor
   */
  EmptyCell(int x, int y):super(x,y);

  /**
   * Class constructor without specific coordinates
   */
  EmptyCell.empty():super(0,0);

  /**
   * Returns the image path of the cell
   */
  @override
  String getImgPath() => _IMG_PATH;

  /**
   * Returns the name of the resource
   */
  @override
  String getResourceName() => _NAME;

  /**
   * Returns the frequency for the empty cell to appear
   */
  @override
  int getFrequency() => 100;

  /**
   * Creates a new empty cell instance without specific coordinates
   */
  @override
  EmptyCell createNewInstance() => EmptyCell.empty();

}

