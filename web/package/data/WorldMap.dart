import 'dart:math';

import '../exception/InvalidActionException.dart';
import 'Base.dart';
import 'Cell.dart';
import 'Mine.dart';
import 'Mountain.dart';
import 'Player.dart';
import '../exception/InvalidDirectionException.dart';
import '../util/Direction.dart';

/**
 * The game map of the world
 */
class WorldMap{

  /**
   * The number of cells for the x axis
   */
  static const NB_CELL_X = 20;

  /**
   * The number of cells for the y axis
   */
  static const NB_CELL_Y = 20;

  /**
   * The list containing all the world cells
   */
  var _world = <List<Cell>>[];

  /**
   * The player
   */
  Player player;

  /**
   * The player's base
   */
  Base base;

  /**
   * Class constructor
   */
  WorldMap(Function generateWorld){
    _initWorld(generateWorld(NB_CELL_X,NB_CELL_Y));
    _placeBase();
    this.player = Player(this.base.x, this.base.y);
  }

  /**
   * Initializes the world with its cells at their right position
   */
  void _initWorld(List<Cell> worldCells) {
    int count = 0;
    for(var y = 0; y < NB_CELL_Y; y++){
      var worldLine = <Cell>[];
      for(var x = 0; x < NB_CELL_X; x++){
        Cell cell = worldCells[count];
        cell.x=x;
        cell.y=y;
        worldLine.add(cell);
        count++;
      }
      _world.add(worldLine);
    }
  }

  /**
   * Puts the base in the map
   */
  void _placeBase(){
    var random = new Random();
    var x = random.nextInt(NB_CELL_X);
    var y = random.nextInt(NB_CELL_Y);
    base = Base(x,y);
    _world[y][x] = base;
  }

  /**
   * Returns the list of cells
   */
  List<List<Cell>> getWorld() => _world;

  /**
   * Gets a cell for specific coordinates
   */
  Cell getWorldCell(int x, int y) => _world[y][x];

  /**
   * Returns the coordinates for the player
   */
  Cell getPlayerCell() => getWorldCell(player.x, player.y);

  /**
   * Returns if the cell in parameter is a mountain
   */
  bool _isMountain(int x, int y) => getWorldCell(x, y) is Mountain;

  /**
   * Returns if the player is on a mine
   */
  bool _playerIsOnMine() => getPlayerCell() is Mine;

  /**
   * Returns if the player is at base
   */
  bool _playerIsAtBase() => player.x == base.x && player.y == base.y;

  /**
   * Moves the player from one cell in a direction
   */
  void doActionPlayerMove(Direction direction){
      int x = player.x;
      int y = player.y;
      switch(direction){
        case Direction.NORTH:
          if (y == 0 || _isMountain(x, y-1)) throw InvalidDirectionException(direction);
          else player.y--;
          break;
        case Direction.EAST:
          if( x == NB_CELL_X-1 || _isMountain(x+1, y)) throw InvalidDirectionException(direction);
          else player.x++;
          break;
        case Direction.SOUTH:
          if( y == NB_CELL_Y-1 || _isMountain(x, y+1))  throw InvalidDirectionException(direction);
          else player.y++;
          break;
        case Direction.WEST:
          if (x == 0 || _isMountain(x-1, y)) throw InvalidDirectionException(direction);
          else player.x--;
          break;
        default:
          throw InvalidDirectionException(direction);
      }
  }

  /**
   * Calls the mining method of the player
   */
  void doActionPlayerMine(){
    if(_playerIsOnMine()){
      player.doActionMine(getPlayerCell());
    }else{
      throw InvalidActionException();
    }
  }

  /**
   * Calls the discharging method of the player
   */
  void doActionPlayerDischarge(){
    if(_playerIsAtBase()){
      player.doActionDischargeOresToBase(getPlayerCell());
    }else{
      throw InvalidActionException();
    }
  }


}