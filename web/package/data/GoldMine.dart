import 'Mine.dart';
import '../util/Ore.dart';

/**
 * A gold mine is a mine composed of gold ores
 */
class GoldMine extends Mine {
  /**
   * The image path of the gold mine
   */
  static const _IMG_PATH = "images/gold.png";
  /**
   * The name of the resource
   */
  static const _NAME ="gold";
  /**
   * The resource type
   */
  static const _TYPE = Ore.GOLD;
  /**
   * The initial resource quantity in the mine
   */
  static const _INITIAL_RESOURCE_QUANTITY = 5;
  /**
   * The frequency for the mine to appear
   */
  static const _FREQUENCY = 6;

  /**
   * Class constructor
   */
  GoldMine(int x, int y):super(_INITIAL_RESOURCE_QUANTITY, x, y );
  /**
   * Class constructor for a gold mine without specific coordinates
   */
  GoldMine.empty():super(_INITIAL_RESOURCE_QUANTITY,0,0);

  /**
   * Returns the image path of the mine
   */
  @override
  String getImgPath() {
    return _IMG_PATH;
  }

  /**
   * Returns the ore type of the gold mine
   */
  Ore getOreType() {
    return _TYPE;
  }

  /**
   * Returns the number of resources that can be mined
   */
  int getMinedCount() {
    return 1;
  }

  /**
   * Returns the frequency for the mine to appear in the map
   */
  @override
  int getFrequency() => _FREQUENCY;

  /**
   * Returns the name of the ore resource
   */
  @override
  String getResourceName() => _NAME;

  /**
   * Creates a new gold mine instance without specific coordinates
   */
  @override
  createNewInstance() => GoldMine.empty();



}