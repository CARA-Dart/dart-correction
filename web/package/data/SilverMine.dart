import 'Mine.dart';
import '../util/Ore.dart';

/**
 * A silver mine is a mine composed of silver ores
 */
class SilverMine extends Mine<SilverMine> {

  /**
   * The image path of the silver mine
   */
  static const _IMG_PATH = "images/silver.png";
  /**
   * The name of the resource
   */
  static const _NAME = "silver";
  /**
   * The resource type
   */
  static const TYPE = Ore.SILVER;
  /**
   * The initial resource quantity in the mine
   */
  static const INITIAL_RESOURCE_QUANTITY = 8;
  /**
   * The frequency for the mine to appear
   */
  static const FREQUENCY = 8;


  /**
   * Class constructor
   */
  SilverMine(int x, int y) :super(INITIAL_RESOURCE_QUANTITY, x, y);
  /**
   * Class constructor for a gold mine without specific coordinates
   */
  SilverMine.empty() :super(INITIAL_RESOURCE_QUANTITY,0,0);

  /**
   * Returns the image path of the mine
   */
  @override
  String getImgPath() => _IMG_PATH;

  /**
   * Returns the ore type of the silver mine
   */
  Ore getOreType() {
    return TYPE;
  }

  /**
   * Returns the number of resources that can be mined
   */
  int getMinedCount() {
    return 2;
  }

  /**
   * Returns the frequency for the mine to appear in the map
   */
  @override
  int getFrequency() => FREQUENCY;

  /**
   * Returns the name of the ore resource
   */
  @override
  String getResourceName() => _NAME;


  /**
   * Creates a new silver mine instance without specific coordinates
   */
  @override
  SilverMine createNewInstance() => SilverMine.empty();

}
