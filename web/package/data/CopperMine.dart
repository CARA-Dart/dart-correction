import 'Mine.dart';
import '../util/Ore.dart';

/**
 * A copper mine is a mine composed of copper ores
 */
class CopperMine extends Mine{

  /**
   * The copper mine image path
   */
  static const _IMG_PATH = "images/copper.png";

  /**
   * The name of the resource
   */
  static const _NAME = "copper";

  /**
   * The ore type
   */
  static const _TYPE = Ore.COPPER;

  /**
   * The initial resource quantity in the mine
   */
  static const _INITIAL_RESOURCE_QUANTITY = 10;

  /**
   * The frequency for the mine to appear
   */
  static const _FREQUENCY = 12;

  /**
   * Class constructor
   */
  CopperMine(int x, int y):super(_INITIAL_RESOURCE_QUANTITY,x,y);

  /**
   * Class constructor to initialize the mine without defined coordinates
   */
  CopperMine.empty():super(_INITIAL_RESOURCE_QUANTITY,0,0);

  /**
   * Returns the image path of the mine
   */
  @override
  String getImgPath() {
    return _IMG_PATH;
  }

  /**
   * Returns the ore type of the copper mine
   */
  Ore getOreType() {
    return _TYPE;
  }

  /**
   * Returns the number of resources that can be mined
   */
  int getMinedCount() {
    return 3;
  }

  /**
   * Returns the frequency for the mine to appear in the game map
   */
  @override
  int getFrequency() {
    return _FREQUENCY;
  }

  /**
   * Returns the name of the ore resource
   */
  @override
  String getResourceName() {
    return _NAME;
  }

  /**
   * Creates an empty instance of copper mine
   */
  @override
  createNewInstance() => CopperMine.empty();

}