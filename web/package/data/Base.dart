import '../util/Ore.dart';
import 'Cell.dart';

class Base extends Cell{

  /**
   * Path the the base image
   */
  static const _IMG_PATH = "images/base.png";

  /**
   * Name of the cell
   */
  static const _NAME = "base";

  /**
   * The inventory of the base, that can contain ores
   */
  var baseInventory = Map();

  /**
   * Class constructor
   */
  Base(int x, int y): super(x,y);

  /**
   * Returns the image path of the base
   */
  @override
  String getImgPath() => _IMG_PATH;

  /**
   * Returns the name of the resource
   */
  @override
  String getResourceName() => _NAME;

  /**
   * Adds an ore to the base inventory
   */
  void add(Ore type, int nbr) {
    if(baseInventory.containsKey(type)) {
      baseInventory[type] += nbr;
    }else{
      baseInventory[type] = nbr;
    }
  }
}