import 'data/Cell.dart';
import 'interfaces/IFrequency.dart';

/**
 * A factory used to generate the list of component of the map
 */
class CellFactory {

  /**
   * Generates the list of component of the map
   */
  static List<Cell> generateListComponent(Set<IFrequency> cellType, int sizeX, int sizeY) {

    var worldCells = new List<Cell>();
    var componentsSum=0;

    cellType.forEach((f) {
      componentsSum+= f.getFrequency();
    });

    cellType.forEach((f) {
      double percentage = ((f.getFrequency() / componentsSum));
      int compoNb = ((sizeX * sizeY) * percentage).round();
      for(int i=0;i<compoNb;i++) {
        worldCells.add(f.createNewInstance());
      }
    });

    worldCells.shuffle();
    return worldCells;
  }
}