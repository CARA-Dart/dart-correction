/**
 * An interface implementing the methods to get an image path and resource name for a cell
 */
abstract class IDisplayResource {
  String getImgPath();
  String getResourceName();
}