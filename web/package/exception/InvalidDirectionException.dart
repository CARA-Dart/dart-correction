import '../util/Direction.dart';

/**
 * Exception occurring when the player cannot move in a direction
 */
class InvalidDirectionException implements Exception{

  Direction direction;

  InvalidDirectionException(this.direction);

  @override
  String toString() => "You can use this directory";
}