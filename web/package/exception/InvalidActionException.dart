/**
 * Exception occurring  when the player attempts an invalid action
 */
class InvalidActionException implements Exception{

  InvalidActionException();

  @override
  String toString() => "You can use this action";
}