/**
 * Exception occurring when a mine is empty
 */
class EmptyMineException implements Exception{

  EmptyMineException();

  @override
  String toString() => "This mine is empty";

}