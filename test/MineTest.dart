import "package:test/test.dart";

import './mock/MineMock.dart';

main(){
  group("Mine", () {
    test("mineOre(...) is empty", () {
      MineMock mineMock = MineMock.empty();
      mineMock.isEmpty = true;
      expect(mineMock.mineOre(10), equals(0));
    });
    test("mineOre(can be mine negative value)", () {
      MineMock mineMock = MineMock.empty();
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(-1), equals(0));
    });
    test("mineOre(can be mine = 0)", () {
      MineMock mineMock = MineMock.empty();
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(0), equals(0));
    });
    test("mineOre(can be mine positive value) mine cout = 0", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 0;
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(5), equals(0));
    });
    test("mineOre(can be mine positive value) mine cout > 0  && resourceQuantity = 0", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 10;
      mineMock.resourceQuantity = 0;
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(5), equals(0));
    });
    test("mineOre(can be mine positive value) mine cout > 0 && resourceQuantity > 0 && can be mine < resourceQuantity", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 10;
      mineMock.resourceQuantity = 10;
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(5), equals(5));
    });
    test("mineOre(can be mine positive value) mine cout > 0 && resourceQuantity > 0 && can be mine > resourceQuantity && minecount < can be mine", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 5;
      mineMock.resourceQuantity = 10;
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(15), equals(mineMock.minecount));
    });
    test("mineOre(can be mine positive value) mine cout > 0 && resourceQuantity > 0 && can be mine >= resourceQuantity && minecount >= can be mine", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 15;
      mineMock.resourceQuantity = 10;
      mineMock.isEmpty = false;
      expect(mineMock.mineOre(15), equals(10));
    });
    test("mineOre(can be mine positive value) mine cout > 0 && resourceQuantity > 0 && can be mine >= resourceQuantity && minecount >= can be mine", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 15;
      mineMock.resourceQuantity = 10;
      mineMock.isEmpty = false;
      mineMock.mineOre(15);
      expect(0, mineMock.resourceQuantity);
    });
    test("mineOre(can be mine positive value) mine cout > 0 && resourceQuantity > 0 && can be mine >= resourceQuantity && minecount >= can be mine", () {
      MineMock mineMock = MineMock.empty();
      mineMock.minecount = 15;
      mineMock.resourceQuantity = 10;
      mineMock.isEmpty = false;
      mineMock.mineOre(15);
      expect(true, mineMock.isEmpty);
    });
  });

}
